package com.appetiser.compositebuilddemo.plugins

object Versions {
  // AndroidX
  const val CORE_KTX = "1.8.0"
  const val APP_COMPAT = "1.5.1"
  const val CONSTRAINT_LAYOUT = "2.1.4"
  const val JUNIT_EXT = "1.1.3"
  const val ESPRESSO_CORE = "3.4.0"

  // Google
  const val MATERIAL = "1.6.1"

  // Third Party Libraries
  const val TIMBER = "5.0.1"
  const val JUNIT = "4.13.2"
}
