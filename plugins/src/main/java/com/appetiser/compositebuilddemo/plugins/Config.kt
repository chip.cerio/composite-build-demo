package com.appetiser.compositebuilddemo.plugins

object Config {
  const val COMPILE_SDK = 32
  const val MIN_SDK = 26
  const val TARGET_SDK = 32
}