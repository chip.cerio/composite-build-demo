package com.appetiser.compositebuilddemo.plugins

object ThirdPartyLibs {
  const val TIMBER = "com.jakewharton.timber:timber:${Versions.TIMBER}"
  const val JUNIT = "junit:junit:${Versions.JUNIT}"
}