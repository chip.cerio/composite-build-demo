package com.appetiser.compositebuilddemo.plugins

object AndroidX {

  const val CORE_KTX = "androidx.core:core-ktx:${Versions.CORE_KTX}"
  const val APP_COMPAT = "androidx.appcompat:appcompat:${Versions.APP_COMPAT}"
  const val CONSTRAINT_LAYOUT = "androidx.constraintlayout:constraintlayout:${Versions.CONSTRAINT_LAYOUT}"
  const val JUNIT_EXT = "androidx.test.ext:junit:${Versions.JUNIT_EXT}"
  const val ESPRESSO_CORE = "androidx.test.espresso:espresso-core:${Versions.ESPRESSO_CORE}"
}