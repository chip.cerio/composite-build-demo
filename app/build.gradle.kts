import com.appetiser.compositebuilddemo.plugins.AndroidX
import com.appetiser.compositebuilddemo.plugins.Config
import com.appetiser.compositebuilddemo.plugins.Google
import com.appetiser.compositebuilddemo.plugins.ThirdPartyLibs

plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("org.jlleitschuh.gradle.ktlint")
}

android {
    compileSdk = Config.COMPILE_SDK

    defaultConfig {
        minSdk = Config.MIN_SDK
        targetSdk = Config.TARGET_SDK
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = true
            isDebuggable = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }

        getByName("debug") {
            isDebuggable = true
            applicationIdSuffix = ".debug"
        }

        create("staging") {
            isDebuggable = true
            applicationIdSuffix = ".staging"
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {
    implementation(AndroidX.CORE_KTX)
    implementation(AndroidX.APP_COMPAT)
    implementation(Google.MATERIAL)
    implementation(AndroidX.CONSTRAINT_LAYOUT)
    implementation(ThirdPartyLibs.JUNIT)
    implementation(AndroidX.JUNIT_EXT)
    implementation(AndroidX.ESPRESSO_CORE)

    implementation(ThirdPartyLibs.TIMBER)
}

ktlint {
    android.set(true)
    outputColorName.set("RED")
}
