Demo on handling dependencies for multi module project using Gradle Composite Builds

[Sample project](https://github.com/henryhuang1219/Android-Util)

[Github search topic](https://github.com/topics/composite-builds)

## Problem

Any change in `buildSrc` invalidates the whole project. Stop using Gradle `buildSrc`. Use composite builds instead. Read on this [article](https://proandroiddev.com/stop-using-gradle-buildsrc-use-composite-builds-instead-3c38ac7a2ab3) for more details.

## Solution: Gradle composite build

Detailed explanation [here](https://proandroiddev.com/dependencies-in-gradle-composite-build-7dca835a46a8)
